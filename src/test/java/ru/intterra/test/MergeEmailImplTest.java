package ru.intterra.test;

import org.junit.Assert;
import org.junit.Test;
import ru.intterra.test.provider.UserWithEmailsProvider;
import ru.intterra.test.merge.MergeUsers;

import java.util.*;

import static org.junit.Assert.*;

public class MergeEmailImplTest {

    @Test
    public void test(){
        Map<String, List<String>> users = new UserWithEmailsProvider(() -> Arrays.asList(
                "user1 -> xxx@ya.ru, foo@gmail.com, lol@mail.ru",
                "user2 -> foo@gmail.com, ups@pisem.net",
                "user3 -> xyz@pisem.net, vasya@pupkin.com",
                "user4 -> ups@pisem.net, aaa@bbb.ru",
                "user5 -> xyz@pisem.net"
        )).getData();

        Map<String, Set<String>> result = new MergeUsers().merge(users);

        assertEquals(2, result.size());
        Assert.assertEquals(asSet("aaa@bbb.ru", "ups@pisem.net", "lol@mail.ru", "xxx@ya.ru", "foo@gmail.com"), result.get("user1"));
        Assert.assertEquals(asSet("vasya@pupkin.com", "xyz@pisem.net"), result.get("user3"));
    }

    @Test
    public void test2() {
        Map<String, List<String>> users = new UserWithEmailsProvider(() -> Arrays.asList(
                "user1 -> t1@ya.ru, t2@ya.ru",
                "user2 -> t3@ya.ru, t4@ya.ru",
                "user3 -> t1@ya.ru, t3@ya.ru"
        )).getData();

        Map<String, Set<String>> result = new MergeUsers().merge(users);

        Assert.assertEquals(asSet("t1@ya.ru", "t2@ya.ru", "t3@ya.ru", "t4@ya.ru"), result.get("user1"));
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void test3() {
        Map<String, List<String>> users = new UserWithEmailsProvider(() -> Arrays.asList(
                "user1 -> t1@ya.ru, t2@ya.ru",
                "user2 -> t3@ya.ru, t4@ya.ru",
                "user3 -> t5@ya.ru, t6@ya.ru",
                "user4 -> t5@ya.ru, t1@ya.ru",
                "user5 -> t7@ya.ru, t8@ya.ru",
                "user6 -> t9@ya.ru, t10@ya.ru",
                "user7 -> t11@ya.ru, t12@ya.ru",
                "user8 -> t7@ya.ru, t12@ya.ru",
                "user9 -> t3@ya.ru, t9@ya.ru",
                "user10 -> t5@ya.ru, t12@ya.ru",
                "user11 -> t12@ya.ru, t9@ya.ru"
        )).getData();

        Map<String, Set<String>> result = new MergeUsers().merge(users);

        Assert.assertEquals(1, result.size());
        Assert.assertEquals(asSet(
                "t1@ya.ru",
                "t2@ya.ru",
                "t3@ya.ru",
                "t4@ya.ru",
                "t5@ya.ru",
                "t6@ya.ru",
                "t7@ya.ru",
                "t8@ya.ru",
                "t9@ya.ru",
                "t10@ya.ru",
                "t11@ya.ru",
                "t12@ya.ru"
                ),
                result.get("user3")
        );
    }

    @Test
    public void test4() {
        Map<String, List<String>> users = new UserWithEmailsProvider(() -> Arrays.asList(
                "user1 -> t111@ya.ru, t222@ya.ru",
                "user2 -> t3@ya.ru, t4@ya.ru",
                "user3 -> t5@ya.ru, t6@ya.ru",
                "user4 -> t5@ya.ru, t1@ya.ru",
                "user5 -> t7@ya.ru, t8@ya.ru",
                "user6 -> t9@ya.ru, t10@ya.ru",
                "user7 -> t11@ya.ru, t12@ya.ru",
                "user8 -> t7@ya.ru, t12@ya.ru",
                "user9 -> t3@ya.ru, t9@ya.ru",
                "user10 -> t5@ya.ru, t12@ya.ru",
                "user11 -> t12@ya.ru, t9@ya.ru"
        )).getData();

        Map<String, Set<String>> result = new MergeUsers().merge(users);

        Assert.assertEquals(asSet(
                "t1@ya.ru",
                "t3@ya.ru",
                "t4@ya.ru",
                "t5@ya.ru",
                "t6@ya.ru",
                "t7@ya.ru",
                "t8@ya.ru",
                "t9@ya.ru",
                "t10@ya.ru",
                "t11@ya.ru",
                "t12@ya.ru"
                ),
                result.get("user3")
        );
        Assert.assertEquals(asSet("t111@ya.ru", "t222@ya.ru"), result.get("user1"));
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void test5() {
        Map<String, List<String>> users = new HashMap<>();
        users.put("user1", null);

        Map<String, Set<String>> result = new MergeUsers().merge(users);

        Assert.assertEquals(new HashSet<>(), result.get("user1"));
        Assert.assertEquals(1, result.size());

    }

    private Set<String> asSet(String... strings) {
        return new HashSet<>(Arrays.asList(strings));
    }

}