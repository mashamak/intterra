package ru.intterra.test.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class StdInReader implements InputReader {

    @Override
    public List<String> getSourceLines() {
        System.out.println("Input data:");
        List<String> inputUserList = new ArrayList<>();
        try (BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in))) {
            String line;
            while (!(line = stdin.readLine()).equals("")) {
                if (!line.isEmpty()) {
                    inputUserList.add(line);
                }
            }
        } catch (IOException e) {
            System.out.println("IOException occurred while reading from StdIn");
        }
        return inputUserList;
    }
}