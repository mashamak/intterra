package ru.intterra.test.io;

import java.util.List;

public interface InputReader {

    List<String> getSourceLines();

}
