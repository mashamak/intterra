package ru.intterra.test.io;

import java.util.Map;
import java.util.Set;

public interface OutputWriter {

    void write(Map<String, Set<String>> data);
}
