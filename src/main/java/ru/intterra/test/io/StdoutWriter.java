package ru.intterra.test.io;

import java.util.Map;
import java.util.Set;

public class StdoutWriter implements OutputWriter {
    @Override
    public void write(Map<String, Set<String>> data) {
        System.out.println("Merged data:");
        data.forEach(
                (key, value) -> System.out.println(String.format("%s -> %s", key, String.join(", ", value))));
    }
}
