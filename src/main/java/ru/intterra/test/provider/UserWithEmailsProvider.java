package ru.intterra.test.provider;

import ru.intterra.test.io.InputReader;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserWithEmailsProvider extends AbstractDataProvider {

    final String LINE_REGEXP = "(?<user>\\w+) -> (?<emailList>[\\w\\.,@ ]+)";
    final String EMAIL_REGEXP = "\\w+@\\w+\\.\\w+";
    final Pattern linePattern = Pattern.compile(LINE_REGEXP);
    final Pattern emailPattern = Pattern.compile(EMAIL_REGEXP);

    public UserWithEmailsProvider(InputReader inputReader) {
        super(inputReader);
    }

    @Override
    protected Map<String, List<String>> parseLines(List<String> sourceLines) {
        HashMap<String, List<String>> result = new LinkedHashMap<>();
        sourceLines.forEach(inputLine -> {
            try {
                Matcher mLine = linePattern.matcher(inputLine);
                mLine.find();
                String user = mLine.group("user");
                String emails = mLine.group("emailList");
                List<String> emailsList = new LinkedList<>();
                Matcher mEmails = emailPattern.matcher(emails);
                while (mEmails.find()) {
                    String email = mEmails.group();
                    emailsList.add(email);
                }
                result.put(user, emailsList);
            } catch (IllegalStateException e) {
                throw new IllegalArgumentException(
                        String.format("Invalid input line: '%s'.", inputLine)
                );
            }
        });
        return result;
    }
}
