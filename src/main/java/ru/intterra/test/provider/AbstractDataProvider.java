package ru.intterra.test.provider;

import ru.intterra.test.io.InputReader;

import java.util.*;

public abstract class AbstractDataProvider implements DataProvider {

    private final InputReader inputReader;

    public AbstractDataProvider(InputReader inputReader){
        this.inputReader = inputReader;
    }

    @Override
    public final Map<String, List<String>> getData() {
        List<String> sourceLines = inputReader.getSourceLines();
        return parseLines(sourceLines);
    }

    protected abstract Map<String, List<String>> parseLines(List<String> sourceLines);
}
