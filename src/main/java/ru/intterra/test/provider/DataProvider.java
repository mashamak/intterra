package ru.intterra.test.provider;

import java.util.List;
import java.util.Map;

public interface DataProvider {

    Map<String, List<String>> getData();
}
