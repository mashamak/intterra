package ru.intterra.test.merge;

import java.util.*;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Optional.ofNullable;

public class MergeUsers implements MergeAlgorithm {

    @Override
    public Map<String, Set<String>> merge(Map<String, List<String>> userEmailsMap) {
        if (userEmailsMap.isEmpty()) {
            return emptyMap();
        }
        if (userEmailsMap.containsKey(null)) {
            throw new IllegalArgumentException("User name can not be null");
        }

        Map<String, String> email2Name = new HashMap<>();
        Map<String, String> user2user = new HashMap<>();

        for (Map.Entry<String, List<String>> userEmails : userEmailsMap.entrySet()) {
            String currentUserName = userEmails.getKey();
            List<String> emails = ofNullable(userEmails.getValue()).orElse(emptyList());
            for (String email : emails) {
                String sameUserName = email2Name.putIfAbsent(email, currentUserName);
                if (sameUserName != null && !sameUserName.equals(currentUserName)) {
                    String conflictInitialUser = getInitialUser(sameUserName, user2user);
                    if (user2user.containsKey(currentUserName)) {
                        String currentInitialUser = user2user.get(currentUserName);
                        if (!conflictInitialUser.equals(currentInitialUser)) {
                            user2user.put(conflictInitialUser, currentInitialUser);
                        }
                    } else {
                        user2user.put(currentUserName, conflictInitialUser);
                    }
                } else if (sameUserName == null && !user2user.containsKey(currentUserName)) {
                    user2user.put(currentUserName, null);
                }
            }
        }

        Map<String, Set<String>> result = new HashMap<>();
        userEmailsMap.forEach((user, emails) -> {
            String initial = getInitialUser(user, user2user);
            result.computeIfAbsent(initial, k -> new HashSet<>()).addAll(ofNullable(emails).orElse(emptyList()));
        });

        return result;
    }

    private String getInitialUser(String name, Map<String, String> user2userMap) {
        String next = user2userMap.get(name);
        if (next == null) {
            return name;
        }
        return getInitialUser(next, user2userMap);
    }

}
