package ru.intterra.test.merge;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface MergeAlgorithm {

    Map<String, Set<String>> merge(Map<String, List<String>> usersInfo);
}
