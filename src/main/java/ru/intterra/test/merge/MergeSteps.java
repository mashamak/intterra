package ru.intterra.test.merge;

import ru.intterra.test.io.OutputWriter;
import ru.intterra.test.provider.DataProvider;

import java.util.List;
import java.util.Map;
import java.util.Set;

public final class MergeSteps {

    private final DataProvider dataProvider;
    private final MergeAlgorithm mergeAlgorithm;
    private final OutputWriter outputWriter;

    public MergeSteps(DataProvider dataProvider, MergeAlgorithm mergeAlgorithm, OutputWriter outputWriter) {
        this.dataProvider = dataProvider;
        this.mergeAlgorithm = mergeAlgorithm;
        this.outputWriter = outputWriter;
    }

    public void start() {
        Map<String, List<String>> sourceData = dataProvider.getData();
        Map<String, Set<String>> mergedData = mergeAlgorithm.merge(sourceData);
        outputWriter.write(mergedData);
    }
}
