package ru.intterra.test;

import ru.intterra.test.io.*;
import ru.intterra.test.merge.MergeAlgorithm;
import ru.intterra.test.merge.MergeSteps;
import ru.intterra.test.merge.MergeUsers;
import ru.intterra.test.provider.DataProvider;
import ru.intterra.test.provider.UserWithEmailsProvider;

public class Application {

    public static void main(String[] args) {
        InputReader inputReader = new StdInReader();
        DataProvider dataProvider = new UserWithEmailsProvider(inputReader);
        MergeAlgorithm mergeAlgorithm = new MergeUsers();
        OutputWriter outputWriter = new StdoutWriter();
        new MergeSteps(dataProvider, mergeAlgorithm, outputWriter).start();
    }
}
